function get_cells() {
    return document.getElementsByClassName('cell');
}
function get_grid() {
    var cells = get_cells();
    var grid = []
    for (var i = 0; i < cells.length; i++) {
	    grid.push(cells[i].innerHTML);
    }
    return grid;
}
function set_grid(grid) {
    var cells = get_cells();
    for (var i = 0; i < cells.length; i++) {
	    cells[i].innerHTML = grid[i];
    }
}
function player_click() {
    fetch('/ttt/play', {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ "move": this.id })
    })
    .then(res => res.json())
    .then(json => set_grid(json.grid));
}
var cells = get_cells();
for (var i = 0; i < cells.length; i++) {
    cells[i].addEventListener('click', player_click, false);
}

