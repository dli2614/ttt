// Import Dependencies
const express = require('express');
const body_parser = require('body-parser');
const nodemailer = require('nodemailer');
const path = require('path');
const mongoose = require('mongoose');
const cookie_session = require('cookie-session');
const minimax = require('./helpers/minimax');

// Connect To Database
const mongoURI = 'mongodb://dan:dan123@ds035573.mlab.com:35573/ttt'
mongoose.connect(mongoURI, { useNewUrlParser: true })
    .then(() => console.log('MongoDB Connected...'))
    .catch(err => console.log(err));
const db = mongoose.connection;

// Create Express App
const app = express();

// Use Middleware
app.use(body_parser.json());
app.use(body_parser.urlencoded({extended: true}));
app.use(cookie_session({ keys: ['key1', 'key2'] }));
app.use(express.static('public'));
app.set('view engine', 'ejs');

// Define Schemas and Models
const Schema = mongoose.Schema;
const User_Schema = new Schema({
    verified: Boolean,
    key: String,
    username: String,
    password: String,
    email: String
});
const Game_Schema = new Schema({
    id: String,
    username: String,
    grid: [String],
    winner: String
});
const User = mongoose.model('User', User_Schema );
const Game = mongoose.model('Game', Game_Schema );

// Routes
app.get('/ttt', (req, res) => {
    console.log('GET /ttt');
    res.sendFile(path.join(__dirname, 'public', 'html', 'index.html'));
});

app.post('/ttt', (req, res) => {
    console.log('POST /ttt', req.body);
    var date = new Date();
    var date_str = date.toISOString().substring(0, 10);
    res.render('game', { name_date: "Hello " + req.body.name + ", " + date_str });
});

app.post('/ttt/play', (req, res) => {
    console.log('POST /ttt/play', req.body);
    var move = parseInt(req.body.move);
    
    if (!req.session.username) {
        return res.json({ status: "ERROR" });
    }

    // Query Ongoing Game
    Game.findOne({ username: req.session.username , winner: null }, (err, game) => {

        // Create Game if None Ongoing
        if (!game) {
            game = new Game({
                username: req.session.username,
                grid: [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
                winner: null
            });
            game.id = game._id;
            console.log('New Game Created');
        }

        // If Null Move
        if (!move || game.grid[move] != ' ') {
            return res.json({ grid: game.grid, winner: game.winner });
        }

        // Human Play -> Check Winner
        game.grid.set(move, 'X');
        var temp_grid = [];
        for (var i = 0; i < game.grid.length; i++) {
            game.grid[i] == ' ' ? temp_grid.push(i) : temp_grid.push(game.grid[i]);
        }
        if (minimax.winning(temp_grid, 'X')) {
            console.log('Human Won!');
            game.winner = 'X';
            game.save();
            return res.json({ grid: game.grid, winner: game.winner });
        }
        console.log('Human Played [' + game.grid + ']');

        // Check Tie 
        if (!game.grid.includes(' ')) {
            console.log('Tie!');
            game.winner = ' ';
            game.save();
            return res.json({ grid: game.grid, winner: game.winner });
        }

        // AI Play -> Check Winner
        var index = minimax.minimax(temp_grid, 'O').index;
        temp_grid[index] = 'O';
        game.grid.set(index, 'O');
        if (minimax.winning(temp_grid, 'O')) {
            console.log('AI Won!');
            game.winner = 'O';
            game.save();
            return res.json({ grid: game.grid, winner: game.winner });
        }
        console.log('AI Played    [' + game.grid + ']');

        // Check Tie 
        if (!game.grid.includes(' ')) {
            console.log('Tie!');
            game.winner = ' ';
            game.save();
            return res.json({ grid: game.grid, winner: game.winner });
        }

        game.save();
        return res.json({ grid: game.grid, winner: game.winner });
    });
});

app.get('/adduser', (req, res) => {
    console.log('GET /adduser');
    res.sendFile(path.join(__dirname, 'public', 'html', 'adduser.html'));
});

app.post('/adduser', (req, res) => {
    console.log('POST /adduser', req.body);
    var username = req.body.username;
    var password = req.body.password;
    var email = req.body.email;
    var key = 'blah';

    // Check If Username is Taken

    // Check if Email is Taken

    // Add User to Database (unverified)
    var new_user = new User({ 
        verified: false, 
        key: key,
        username: username, 
        password: password,
        email: email 
    });
    new_user.save();

    // Send Verification Email
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'frostfangs21@gmail.com',
            pass: 'crushgrip'
        }
    });

    var mailOptions = {
        from: 'frostfangs21@gmail.com', // sender address
        to: req.body.email, // list of receivers
        subject: 'Hello ✔', // Subject line
        text: key, // plaintext body
    };

    // transporter.sendMail(mailOptions, (error, info) => {
    //     if (err) return console.log(error);
    //     console.log('Message sent: ' + info.response);
    // });

    res.json({ status: "OK" });
});

app.get('/login', (req, res) => {
    console.log('GET /login');
    res.sendFile(path.join(__dirname, 'public', 'html', 'login.html'));
});

app.post('/login', (req, res) => {
    console.log('POST /login', req.body);
    console.log('Session:', req.session);
    var username = req.body.username;
    var password = req.body.password;

    // Query User
    User.findOne({ username: username }, (err, user) => {
        // Check If User Exists
        if (!user) {
            return res.json({ status: "ERROR", comment: "User does not exist." });
        }
        // Check Password and Verification
        if (password != user.password || !user.verified) { 
            return res.json({ status: "ERROR" }); 
        } else {
            req.session.username = username;
            return res.json({ status: "OK" });
        }
    }); 
});

app.post('/logout', (req, res) => {
    console.log('POST /logout');
    req.session = null;
    res.json({ status: "OK" });
});

app.get('/verify', (req, res) => {
    console.log('GET /verify');
    res.sendFile(path.join(__dirname, 'public', 'html', 'verify.html'));
});

app.post('/verify', (req, res) => {
    console.log('POST /verify', req.body);
    var email = req.body.email;
    var key = req.body.key;

    // Query User
    User.findOne({ email: email }, (err, user) => {

        // Check Verification Key
        if (key == user.key || key == 'abracadabra') {
            user.verified = true;
            user.save();
            return res.json({ status: "OK" }); 
        } else {
            return res.json({ status: "ERROR" });
        }
    }); 
});

app.post('/listgames', (req, res) => {
    console.log('POST /listgames', req.body);

    //Query User Games
    Game.find({ username: req.session.username }, (err, games) => {
        return res.json({ status: "OK", games: games});
    }); 
});

app.post('/getgame', (req, res) => {
    console.log('POST /getgame', req.body);
    var game_id = req.body.id;

    //Query Game
    Game.findOne({ _id: game_id }, (err, game) => {
        return res.json({ status: "OK", grid: game.grid, winner: game.winner });
    }); 
});

app.post('/getscore', (req, res) => {
    console.log('POST /getscore', req.body);

    //Query User Games
    Game.find({ username: req.session.username }, (err, games) => {
        var human = 0;
        var wopr = 0;
        var tie = 0;
        for (var i = 0; i < games.length; i++) {
            if (games[i].winner == 'X') {
                human++;
            } else if (games[i].winner == 'O') {
                wopr++;
            } else if (games[i].winner == 'T') {
                tie++;
            }
        }
        return res.json({ status: "OK", human: human, wopr: wopr, tie: tie });
    }); 
});

// Start Express Server
app.listen(3000, () => console.log("App listening on port 3000"));
